﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw16052020
{
    class Program
    {
        static void Main(string[] args)
        {
            var runner = new Linq();
            runner.Run();
            var runnerEvents = new GoLawRunner();
            runnerEvents.Run();
        }
    }
    /// <summary>
    /// просто на 3 балла, сложный на 5 баллов
    /// </summary>
    public class Linq
    {
        public class Product
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        public class Price
        {
            public int Id { get; set; }
            public int ProductId { get; set; }
            public decimal Sum { get; set; }
            public bool IsActual { get; set; }
        }

        public void Run()
        {
            var products = new List<Product>
            {
                new Product { Id = 1, Name = "Аквариум 10 литров" },
                new Product { Id = 2, Name = "Аквариум 20 литров" },
                new Product { Id = 3, Name = "Аквариум 50 литров" },
                new Product { Id = 4, Name = "Аквариум 100 литров" },
                new Product { Id = 5, Name = "Аквариум 200 литров" },
                new Product { Id = 6, Name = "Фильтр" },
                new Product { Id = 7, Name = "Термометр" }
            };

            var prices = new List<Price>
            {
                new Price { Id = 1, ProductId = 1, Sum = 100, IsActual = false },
                new Price { Id = 2, ProductId = 1, Sum = 123, IsActual = true },
                new Price { Id = 3, ProductId = 2, Sum = 234, IsActual = true },
                new Price { Id = 4, ProductId = 3, Sum = 532, IsActual = true },
                new Price { Id = 5, ProductId = 4, Sum = 234, IsActual = true },
                new Price { Id = 6, ProductId = 5, Sum = 534, IsActual = true },
                new Price { Id = 7, ProductId = 5, Sum = 124, IsActual = false },
                new Price { Id = 8, ProductId = 6, Sum = 153, IsActual = true },
                new Price { Id = 9, ProductId = 7, Sum = 157, IsActual = true }
            };

            /*Задания              * 
             * 1) создать список счетов (один счет содержит несколько пар цена-количество)
             * например, один счет - это аквариум на 200 литров, два фильтра и термометр
             */
            var res1 = products.Join(
               prices,
               pro => pro.Id,
               pri => pri.ProductId,
               (pro, pri) => new
               {
                   name = pro.Name,
                   sum = pri.Sum,
                   isActual = pri.IsActual
               }).Where(x => x.isActual.Equals(true)).ToList();
            /*
            * 2) вывести счет для покупателя с колонками "Наименование услуги, сумма, итого"
            * в порядке сортировки по наименованию товара в лексикографическом порядке
            *  - 1 балл
            * 3) Вывести среднюю цену для каждого продукта с учетом неактуальных значений
            * - 1 балл
            */
            foreach (var i in prices)
            {
                var res2 = prices.Average(x => x.Sum);
            }
            /* 4) создать список акций (код продукта, скидка):
            * аквариум на 200 литров + 2 фильтра - скидка 15%
            * аквариум 100 литров + 2 фильтра - 10% скидка
            * любой другой аквариум + фильтр - 5% скидка
            * 5) создать список перечень всех названий товаров в группе акции + 
            * цена до скидки + цена с учетом скидки  - 1 балл
            * 
            * Для тех, кто выбрал вариант посложнее: написать функцию подсчета 
            * суммы покупки (выявлять, есть ли в наборе продуктов акционные комплекты,
            * при их наличии делать скидку), 
            * выводить итого без учета скидки и со скидкой, отельно сумму скидки
            * - это + 2 балла
            * использовать Linq операции со множествами
            */
        }
    }
}
