﻿using System;

/// <summary>
/// Работник (источник события "Выходит закон")
/// </summary>
public class Law
{
    private string _name;

    /// <summary>
    /// Имя сотрудника
    /// </summary>
    public string Name { get { return _name; } }

    public Law(string name)
    {
        _name = name;
    }

    /// <summary>
    /// Выход закона
    /// </summary>
    public event EventHandler<GoLawEventArgs> GoVacationEvent;

    /// <summary>
    /// Добавить закон в приложение?(так в тз написанно,наверное)
    /// </summary>
    public void GoVacation(DateTime from, DateTime to, bool isAdm = false)
    {
        var args = new GoLawEventArgs(from, to, isAdm);

        if (GoVacationEvent != null)
            GoVacationEvent(this, args);
    }
}