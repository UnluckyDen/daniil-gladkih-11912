﻿using System;

/// <summary>
/// Менеджер (ищет замену сотруднику, уходящему в отпуск)
/// </summary>
public class Manager
{
    /// <summary>
    /// Подписаться на событие "Выход закона"
    /// </summary>
    /// <param name="worker">работник</param>
    public void Subscribe(Law law)
    {
        law.GoVacationEvent += FindReplacement;
    }
    /// <summary>
    /// Отменить подписку на событие "Выход закона"
    /// </summary>
    /// <param name="law"></param>
    public void UnSubscribe(Law law)
    {
        law.GoVacationEvent -= FindReplacement;
    }

    /// <summary>
    /// Поиск сотрудника на замену
    /// </summary>
    /// <param name="sender">источник события - Law</param>
    /// <param name="e">аргументы (параметры) события</param>
    private void FindReplacement(object sender, GoLawEventArgs e)
    {
        Console.WriteLine("Осуществляется поиск закона " +
            ((Law)sender).Name + " выпущенного " +
            e.From.ToShortDateString() + " добавленного в ПО " + e.To.ToShortDateString());
    }
}
