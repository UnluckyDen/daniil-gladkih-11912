﻿using System;
using System.Collections.Generic;
using System.Text;

public class GoLawRunner
{
    public void Run()
    {
        var LawMnogodetniy = new Law("*Выплаты для семейс 2 и более детьми*");
        var manager = new Manager();
        var fd = new FinDepartment();

        //никто не подписан
        LawMnogodetniy.GoVacation(new DateTime(2020, 1, 30),
            new DateTime(2020, 2, 12));

        //подписался менеджер
        manager.Subscribe(LawMnogodetniy);
        //подписался фин отдел
        fd.Subscribe(LawMnogodetniy);

        LawMnogodetniy.GoVacation(new DateTime(2020, 2, 17),
            new DateTime(2020, 2, 23), true);

        var LawMaski = new Law("*Выдыча масок всем гражд*");
        //Менеджер отписывается от Mnogodetniy и подписывается на Maski
        manager.UnSubscribe(LawMnogodetniy);
        manager.Subscribe(LawMaski);

        //фин отдел подписывается на Михаила
        fd.Subscribe(LawMaski);

        LawMnogodetniy.GoVacation(new DateTime(2020, 3, 2),
            new DateTime(2020, 3, 8));
        LawMaski.GoVacation(new DateTime(2020, 3, 2),
            new DateTime(2020, 3, 8));
    }
}