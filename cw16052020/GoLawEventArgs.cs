﻿using System;

/// <summary>
/// Параметры выхода закона
/// </summary>
public class GoLawEventArgs : EventArgs
{
    private DateTime _from, _to;
    private bool _isTr;

    public GoLawEventArgs(DateTime from, DateTime to, bool isTr)
    {
        if (from > to) throw new ArgumentException("Дата издания закона" +
            " не может быть больше даты добавления формы в ПО");
        _from = from;
        _to = to;
        _isTr = isTr;
    }

    /// <summary>
    /// Дата начала действия закона
    /// </summary>
    public DateTime From { get { return _from; } }
    /// <summary>
    /// Дата добавления формы в ПО
    /// </summary>
    public DateTime To { get { return _to; } }
    /// <summary>
    /// Закон работает для не многодетных семей?
    /// </summary>
    public bool IsTr { get { return _isTr; } }

}