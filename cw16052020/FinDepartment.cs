﻿using System;
using System.Collections.Generic;
using System.Text;


    /// <summary>
    /// Финансовый отдел, подписчик события "Уход сотрудника в отпуск"
    /// </summary>
    public class FinDepartment
    {
        /// <summary>
        /// Подписаться на событие "Уход работника в отпуск"
        /// </summary>
        /// <param name="law">работник</param>
        public void Subscribe(Law law)
        {
            law.GoVacationEvent += CalcVacationPay;
        }
        /// <summary>
        /// Отменить подписку на событие "Уход работника в отпуск"
        /// </summary>
        /// <param name="law"></param>
        public void UnSubscribe(Law law)
        {
            law.GoVacationEvent -= CalcVacationPay;
        }

        /// <summary>
        /// Рассчитать выплаты
        /// </summary>
        /// <param name="sender">источник события - Law</param>
        /// <param name="e">аргументы (параметры) события</param>
        private void CalcVacationPay(object sender, GoLawEventArgs e)
        {
            Console.WriteLine("Осуществляется написание заявления по форме " +
                ((Law)sender).Name + " на период отсутствия формы в ПО с " +
                e.From.ToShortDateString() + " по " + e.To.ToShortDateString() +
                ", тип семьи " + (e.IsTr ? "многодетная" : "немногодетная"));
        }
    }
