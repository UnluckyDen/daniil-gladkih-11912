﻿/*using System;
using System.Collections.Generic;
using System.Text;

namespace wc16052020
{
    /// <summary>
    /// Узел дерева в общем смысле
    /// </summary>
    public class CommonTreeNode<T>
    {
        /// <summary>
        /// Ключ
        /// </summary>
        public int Key { get; set; }
        /// <summary>
        /// Информационное поле
        /// </summary>
        public T Data { get; set; }
        /// <summary>
        /// Потомки, дочерние элементы
        /// </summary>
        List<CommonTreeNode<T>> Children { get; set; }
        /// <summary>
        /// Родительский узел
        /// </summary>
        CommonTreeNode<T> Parent { get; set; }
    }
}
*/