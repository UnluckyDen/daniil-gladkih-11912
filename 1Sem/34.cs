﻿using System;
/*
* @author Gladkih Daniil
* 11912
* Task 34
*/

namespace _34
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int f = Fibonacci(n);
            Console.WriteLine(f);

        }


        static int Fibonacci(int n)
        {
            int a = 0;
            int b = 1;
            int tmp;

            for (int i = 0; i < n; i++)
            {
                tmp = a;
                a = b;
                b += tmp;
            }

            return a;
        }
    }
}
