﻿using System;
/*
*@author Daniil Gladkih
*11912
*task 40
*/

namespace _40
{
    class Program
    {
        static void Main(string[] args)
        {
            string a1 = Console.ReadLine();
            magic(a1);
        }
        static void magic(string a1)
        {
            int q = 0, w = 0, e = 0, r = 0, t = 0, y = 0, u = 0, i = 0, o = 0, p = 0, a = 0, s = 0, d = 0, f = 0, g = 0, h = 0, j = 0, k = 0, l = 0, z = 0, x = 0, c = 0, v = 0, b = 0, n = 0, m = 0;
            for (int i1 = 0; i1<a1.Length;i1++)
            {
                if ((a1[i1]=='q') || (a1[i1]=='Q'))
                {
                    q++;
                }
                if  (a1[i1] == 'W')
                {
                    w++;
                }
                if  (a1[i1] == 'E')
                {
                    e++;
                }
                if (a1[i1] == 'R')
                {
                    r++;
                }
                if ( (a1[i1] == 'T'))
                {
                    t++;
                }
                if ((a1[i1] == 'Y'))
                {
                    y++;
                }
                if ((a1[i1] == 'U'))
                {
                    u++;
                }
                if ( (a1[i1] == 'I'))
                {
                    i++;
                }
                if ((a1[i1] == 'O'))
                {
                    o++;
                }
                if ((a1[i1] == 'P'))
                {
                    p++;
                }
                if ((a1[i1] == 'A'))
                {
                    a++;
                }
                if ((a1[i1] == 'S'))
                {
                    s++;
                }
                if ((a1[i1] == 'D'))
                {
                    d++;
                }
                if ((a1[i1] == 'F'))
                {
                    f++;
                }
                if ((a1[i1] == 'G'))
                {
                    g++;
                }               
                if ((a1[i1] == 'H'))
                {
                    h++;
                }
                if ((a1[i1] == 'J'))
                {
                    j++;
                }
                if ((a1[i1] == 'K'))
                {
                    k++;
                }
                if ( (a1[i1] == 'L'))
                {
                    l++;
                }
                if ((a1[i1] == 'Z'))
                {
                    z++;
                }
                if ((a1[i1] == 'X'))
                {
                    x++;
                }
                if ((a1[i1] == 'C'))
                {
                    c++;
                }
                if ((a1[i1] == 'V'))
                {
                    v++;
                }
                if ((a1[i1] == 'B'))
                {
                    b++;
                }
                if ((a1[i1] == 'N'))
                {
                    n++;
                }
                if ((a1[i1] == 'M'))
                {
                    m++;
                }             
            }
            Console.WriteLine("q" + q);
            Console.WriteLine("w" + w);
            Console.WriteLine("e" + e);
            Console.WriteLine("r" + r);
            Console.WriteLine("t" + t);
            Console.WriteLine("y"+ y);
            Console.WriteLine("u" + u);
            Console.WriteLine("i" + i);
            Console.WriteLine("o" + o);
            Console.WriteLine("p" + p);
            Console.WriteLine("a" + a);
            Console.WriteLine("s" + s);
            Console.WriteLine("d" + d);
            Console.WriteLine("f" + f);
            Console.WriteLine("g" + g);
            Console.WriteLine("h" + h);
            Console.WriteLine("j" + j);
            Console.WriteLine("k" + k);
            Console.WriteLine("l" + l);
            Console.WriteLine("z" + z);
            Console.WriteLine("x" + x);
            Console.WriteLine("c" + c);
            Console.WriteLine("v" + v);
            Console.WriteLine("b" + b);
            Console.WriteLine("n" + n);
            Console.WriteLine("m" + m);
            Console.WriteLine("Vsego" + (q+w+e+r+t+y+u+i+o+p+a+s+d+f+g+h+j+k+l+z+x+c+v+b+n+m));
        }
    }
}
