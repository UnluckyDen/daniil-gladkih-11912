﻿using System;
/*
*@author Daniil Gladkih
*11912
*task 20
*/

namespace _20
{
    class Program
    {
        static void Main()
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int[] a = new int[n];
            for (int i = 0; i < n; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n - 1; j++)
                {
                    if (a[j] > a[j + 1])
                    {
                        int d = a[j];
                        a[j] = a[j + 1];
                        a[j + 1] = d;
                    }
                }
            }
            for (int k = 1; k < n; k++)
            {
                if (a[k - 1] == a[k])
                {
                    Console.Write("Not ");
                    break;
                }
            }
            Console.WriteLine("unique");
        }
    }
}
