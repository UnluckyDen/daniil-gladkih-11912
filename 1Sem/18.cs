﻿using System;
/*
*@author Daniil Gladkih
*11912
*task 18
*/

namespace _18
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int[] a = new int[n];
            for (int i = 0; i < n; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }
            int b = a[0];
            for (int i = 1; i < n; i++)
            {
                int tmp = a[i];
                while (tmp>0)
                {
                    b *= 10;
                    tmp /= 10;
                }
                b += a[i];
            }
            Console.WriteLine(b);
            int d = a[n - 1];
            for (int i = n - 2; i >= 0; i--)
            {
                int tmp = a[i];
                while (tmp > 0)
                {
                    d *= 10;
                    tmp /= 10;
                }
                d += a[i];
            }
            Console.WriteLine(d);
        }
    }
}