﻿using System;
/*
*@author Daniil Gladkih
*11912
*task 19
*/

namespace _19
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int n = Convert.ToInt32(Console.ReadLine());
            int[] a = new int[n],b = new int[n];
            for(int i=0;i<n;i++)
            {
                a[i] = i;
                b[i] = rnd.Next(0,100);
                a[i] = b[i];
                Console.Write(a[i] + " ");
            }
        }
    }
}
