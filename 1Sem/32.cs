﻿using System;
/*
* @author Gladkih Daniil
* 11912
* Task 32
*/ 

namespace _32
{
    class Program
    {
        static void Main(string[] args)
        {
            double n = Convert.ToDouble(Console.ReadLine());
            n = Factorial(n);
            Console.WriteLine(n);
        }


        static double Factorial(double n)
        {
            int nf = 1;

            for (int i = 1; i <= n; i++)
                nf = nf * i;

            return nf;
        }
    }
}