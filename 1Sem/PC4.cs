﻿using System;
/*
*@author Daniil Gladkih
*11912
*task C4
*/

namespace C4
{
    class C4
    {
        static bool Porydok(int[] a)
        {
            int n = a.Length;
            bool answer = true;
            for (int i = 1; i < n; i++)
            { 
                    if (a[i] < a[i-1])
                        answer = false;
                        break;
                }
            return answer;
        }
        static void Main(string[] args)
        {
            int[] a = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            int n = a.Length;
            bool ans = Porydok(a);
            if (ans==true)
            {
                Console.WriteLine("+");
            }
            else
            {
                Console.WriteLine("-");
            }
            Console.ReadKey();
        }
    }
}
