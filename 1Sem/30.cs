﻿using System;
/*
*@author Daniil Gladkih
*11912
*task 30
*/

namespace _30
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            double[,] m = new double[n, n];
            double[] a = new double[n];
            MatrixSozdanie(ref n, m);
            MatrixPrint(ref m, ref n);
            #region Привеление к треугольному виду
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = i + 1; j < n; j++)
                {
                    double qwe = m[j, i] / m[i, i];
                    for (int k = i; k < n; k++)
                        m[j, k] -= m[i, k] * qwe;
                }
            }
            Console.WriteLine();
            MatrixPrint(ref m, ref n);
            Console.WriteLine(); ;
            Console.WriteLine("Esli matrica ne treugolnay , to ne povezlo");
        #endregion

    }
        static void MatrixSozdanie(ref int n, double[,] m )
        {
            Random random = new Random();
            for (int i = 0; i<n; i++)
            {
                for(int j = 0; j < n;j++)
                {
                    m[i, j] = random.Next(1,10);
                }
            }
        }
        static void MatrixPrint(ref double[,] m, ref int n)
        {
            for (int i = 0; i < (n); i++)
            {
                Console.WriteLine();
                for (int j = 0; j < (n); j++)
                {
                    Console.Write(Math.Round(m[i, j],2) + " ");
                }
            }
        }


    }
}
