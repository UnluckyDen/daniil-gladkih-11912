﻿using System;
/*
*@author Daniil Gladkih
*11912
*task 38
*/

namespace _38
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = Console.ReadLine(), b = Console.ReadLine();
            if (a.Length < b.Length)
            {
                for (int i = 0; i < a.Length; i++)
                {
                    if (Convert.ToInt32(a[i]) < Convert.ToInt32(b[i]))
                    {
                        Console.WriteLine((i + 1) + " " + a[i] + "<" + b[i]);
                    }
                    else
                    {
                        if (Convert.ToInt32(a[i]) > Convert.ToInt32(b[i]))
                        {
                            Console.WriteLine((i + 1) + " " + a[i] + ">" + b[i]);
                        }
                        else Console.WriteLine((i + 1) + " " + a[i] + "=" + b[i]);
                    }
                }
            }
            else
                for (int i = 0; i < b.Length; i++)
                {
                    if (Convert.ToInt32(a[i]) < Convert.ToInt32(b[i]))
                    {
                        Console.WriteLine((i + 1) + " " + a[i] + "<" + b[i]);
                    }
                    else
                    {
                        if (Convert.ToInt32(a[i]) > Convert.ToInt32(b[i]))
                        {

                            Console.WriteLine((i + 1) + " " + a[i] + ">" + b[i]);
                        }
                        else Console.WriteLine((i + 1) + " " + a[i] + "=" + b[i]);
                    }
                }
        }
    }
}
