﻿using System;
/*
*@author Daniil Gladkih
*11912
*task 23
*/

namespace _23
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int n = Convert.ToInt32(Console.ReadLine());
            string x = Convert.ToString(Console.ReadLine());
            int[] a = new int[n];
            for (int i=0;i<n;i++)
            {
                a[i] = i+1;
            }
            int n1 = a.Length + x.Length;
            int[] arr = new int[n1];
            for (int i = 0; i < n ;i++)
            {
                arr[i] += a[i] * Convert.ToInt32(x);
                arr[i + 1] += arr[i] / 10;
                arr[i] %= 10;
            }

            for (int i = n1 - 1; i >= 0; i--)
            {
                Console.Write(arr[i]);
            }
            Console.WriteLine();
        }
    }
}