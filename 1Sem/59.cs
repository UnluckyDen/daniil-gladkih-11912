﻿/*
*@author Daniil Gladkih
*11912
*task 59
*/
using System;

namespace _57
{
    class Program
    {
        static void Main(string[] args)
        {
            Cipher A1 = new ACipher("BbbB");
            Cipher B1 = new BCipher("BbbB");
            Console.WriteLine("---------");
            B1.encode(B1.GetStroka());
            A1.encode(A1.GetStroka());
            Console.WriteLine(A1.code);
            Console.WriteLine("---------");
            Console.WriteLine(B1.code);
            Console.WriteLine("---------");
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
interface ICipher
{
    void encode(string ec);
    void decode(string dc);
}
abstract class Cipher : ICipher
{
    public string GetStroka()
        {
        return this.stroka;
        }
    public Cipher(string stroka)
    {
        this.stroka = stroka;
    }
    public string code;
    public string necode;
    protected string stroka;
    public abstract void encode(string ec);
    public abstract void decode(string dc);
}
class ACipher : Cipher
{
    public ACipher(string stroka) : base(stroka) { }
    public override void encode(string stroka)
    {
        for (int i = 0; i < stroka.Length;i++)
        {
            if (Convert.ToInt32(stroka[i]) != 90 && Convert.ToInt32(stroka[i]) != 122)
                code += Convert.ToChar((Convert.ToInt32(stroka[i] + 1)));
            else
            {
                if (Convert.ToInt32(stroka[i]) == 90)
                    code += "A";
                else
                {
                    if (Convert.ToInt32(stroka[i]) == 122)
                        code += "a";
                }
            }
        }
    }
    public override void decode(string stroka)
    {
        for (int i = 0; i < stroka.Length;i++)
        {
            if (Convert.ToInt32(stroka[i]) != 65 && Convert.ToInt32(stroka[i]) != 97)
                necode += Convert.ToChar((stroka[i] - 1));
            else
            {
                if (Convert.ToInt32(stroka[i]) == 65)
                    necode += "Z";
                else
                {
                    if (Convert.ToInt32(stroka[i]) == 97)
                        necode += "z";
                }
            }
        }
    }
    public string Code
    {
        get { return code; }
    }
    public string NeCode
    {
        get { return necode; }
    }
}
class BCipher : Cipher
{
    public BCipher(string stroka) : base(stroka) { }
    public override void encode(string stroka)
    {
        for (int i = 0; i < stroka.Length; i++)
        {
            if ((Convert.ToInt32(stroka[i]) >= 65) && (Convert.ToInt32(stroka[i]) <= 90))
            {
                code +=Convert.ToChar(90+65-Convert.ToInt32(stroka[i]));
            }
            else if ((Convert.ToInt32(stroka[i]) >= 97) && (Convert.ToInt32(stroka[i]) <= 122))
            {
                code += Convert.ToChar(122 + 97 - Convert.ToInt32(stroka[i]));
            }
        }
    }
    public override void decode(string stroka)
    {
        for (int i = 0; i < stroka.Length; i++)
        {
            if ((Convert.ToInt32(stroka[i]) >= 65) && (Convert.ToInt32(stroka[i]) <= 90))
            {
                necode += Convert.ToChar(90 + 65 - Convert.ToInt32(stroka[i]));
            }
            else if ((Convert.ToInt32(stroka[i]) >= 97) && (Convert.ToInt32(stroka[i]) <= 122))
            {
                necode += Convert.ToChar(122 + 97 - Convert.ToInt32(stroka[i]));
            }
        }
    }
    public string Code
    {
        get { return code; }
    }
    public string NeCode
    {
        get { return necode; }
    }
}
