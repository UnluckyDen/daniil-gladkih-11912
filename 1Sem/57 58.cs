﻿/*
*@author Daniil Gladkih
*11912
*task 57/58
*/
using System;

namespace _57
{
    class Program
    {
        static void Main(string[] args)
        {
            fig p = new point(5,2);
            p.muvmentx(3);
            circle c = new circle(4, 4, 2);
            Console.WriteLine(c.Sq());
            Console.WriteLine(p);
            Console.WriteLine(c);
            fig[] figs = new fig[2];
            figs[0] = p;
            figs[1] = c;
            Console.ReadKey();
        }
    }
    }
        interface IMoveFig
            {
                void muvmentx(int dx);
                void muvmenty(int dy);
            }
        abstract class fig:IMoveFig
        {
            private bool vis;
            private string col;
        public fig() { col = "black"; }
        public fig(bool v,string c)
        {vis = v;col = c;}
        public abstract void muvmentx(int dx);
        public abstract void muvmenty(int dy);
            public override string ToString()
            {
                return "vis=" + vis + ",col=" + col;
            }
            public void ChangCol(string colo) { col = colo; }
            public bool Visabile { get { return vis; } }                
        }
    class point : fig
    {
        private int x, y;
        public point() : base() { }
        public point(int x,int y) { this.x = x; this.y = y; }
        public point(int x,int y,bool v,string c) : base(v, c) { this.x=x;this.y=y; }
        public override void muvmentx(int dx) { x += dx; }
        public override void muvmenty(int dy) { y += dy; }
        public override string ToString()
        {
            return base.ToString() + ",x=" + x + ",y=" + y;
        }
    }
    class circle:point
    {
        private int rad;
        public double Sq() { return 2 * Math.PI * rad * rad; }
        public circle() : base() { }
        public circle(int x, int y,int r) : base(x, y) { rad = r; }
        public circle(int x, int y, bool v, string c,int r) : base(x, y, v, c) { rad=r; }
        public override string ToString()
        {
            return base.ToString() + ",rad = " + rad;
        }
    }
    
