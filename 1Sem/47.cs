﻿using System;
/* 
* @author Gladkih Daniil
* 11912 
* Task 47 
*/

namespace Task47
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = Console.ReadLine();
            int n = a.Length;
            char[] res = new char[n];
            int i = 0, j = 0;
            int p = 1;
            while (i < n)
            {
                switch (p)
                {
                    case 1:
                        if (a[i] == '0')
                        {
                            p = 2;
                            i++;
                        }
                        else if (a[i] == '1')
                        {
                            p = 3;
                            i++;
                        }
                        else
                        {
                            p = 0;
                            i = n;
                        }
                        break;

                    case 2:
                        if (a[i] == '0')
                        {
                            p = 6;
                            i++;
                        }
                        else if (a[i] == '1')
                        {
                            p = 4;
                            i++;
                        }
                        else
                        {
                            p = 0;
                            i = n;
                        }
                        break;

                    case 3:
                        if (a[i] == '1')
                        {
                            p = 7;
                            i++;
                        }
                        else if (a[i] == '0')
                        {
                            p = 5;
                            i++;
                        }
                        else
                        {
                            p = 0;
                            i = n;
                        }
                        break;

                    case 4:
                        if (a[i] == '0')
                        {
                            p = 8;
                            i++;
                        }
                        else if (a[i] != '0')
                        {
                            p = 0;
                            i = n;
                        }
                        break;

                    case 5:
                        if (a[i] == '1')
                        {
                            p = 9;
                            i++;
                        }
                        else if (a[i] != '1')
                        {
                            p = 0;
                            i = n;
                        }
                        break;

                    case 6:
                        if (a[i] == '0')
                        {
                            p = 6;
                            i++;
                        }
                        else if (a[i] != '0')
                        {
                            p = 0;
                            i = n;
                        }
                        break;

                    case 7:
                        if (a[i] == '1')
                        {
                            p = 7;
                            i++;
                        }
                        else if (a[i] != '1')
                        {
                            p = 0;
                            i = n;
                        }
                        break;

                    case 8:
                        if (a[i] == '1')
                        {
                            p = 10;
                            i++;
                        }
                        else if (a[i] != '1')
                        {
                            p = 0;
                            i = n;
                        }
                        break;

                    case 9:
                        if (a[i] == '0')
                        {
                            p = 10;
                            i++;
                        }
                        else if (a[i] != '0')
                        {
                            p = 0;
                            i = n;
                        }
                        break;

                    case 10:
                        if (a[i] == '0')
                        {
                            p = 8;
                            i++;
                        }
                        else if (a[i] == '1')
                        {
                            p = 9;
                            i++;
                        }
                        else if ((a[i] != 0) && (a[i] != 1))
                        {
                            p = 0;
                            i = n;
                        }
                        break;
                }
            }
            if ((p == 4) || (p == 5) || (p == 6) || (p == 7) || (p == 8) || (p == 9) || (p == 10))
                Console.WriteLine("+");
            else
                Console.WriteLine("-");
        }
    }
}