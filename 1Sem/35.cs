﻿using System;
/*
* @author Gladkih Daniil
* 11912
* Task 35
*/

namespace _34
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int f = Fibonacci(n);
            Console.WriteLine(f);

        }


        static int Fibonacci(int n)
        {
            if (n == 0 || n == 1)
            {
                return n;
            }
            else
            {
                return Fibonacci(n - 1) + Fibonacci(n - 2);
            }

        }
    }
}
