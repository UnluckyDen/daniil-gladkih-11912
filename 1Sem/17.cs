﻿/*
*@author Daniil Gladkih
*11912
*task 17
*/
using System;

namespace _17
{
    class Program
    {
        static void Main()
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int[] a = new int[n];
            a[0] = 1;
            Console.WriteLine(a[0]);
            for (int i=1;i<n; i++)
            {
                if (a[i-1]<0)
                {
                    a[i] = -(a[i - 1] - 2);
                    Console.WriteLine(a[i]);
                }
                else
                {
                    a[i] = -(a[i - 1] + 2);
                    Console.WriteLine(a[i]);
                }
            }
        }
    }
}
