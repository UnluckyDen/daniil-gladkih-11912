﻿using System;
/*
*@author Daniil Gladkih
*11912
*task spiral
*/

namespace Spiral
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            n = Convert.ToInt32(Console.ReadLine());
            int[,] arr = new int[n, n];
            Random r = new Random();
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < n; j++)
                {
                    arr[i, j] = r.Next(100);
                    Console.Write(arr[i, j] + " ");
                }
            }
            Console.WriteLine();
            Console.WriteLine();
            int a = 0, b = 1, c = 2;
            for (int i = 0; i < n; i++)
            {
                for (int j = a; j < n - a; j++)
                {
                    Console.Write(arr[a, j] + " ");
                }
                for (int j = b; j < n - a; j++)
                {
                    Console.Write(arr[j, n - b] + " ");
                }
                for (int j = n - c; j >= a; j--)
                {
                    Console.Write(arr[n - b, j] + " ");
                }
                for (int j = n - c; j > a; j--)
                {
                    Console.Write(arr[j, a] + " ");
                }
                a++;
                b++;
                c++;
            }
            Console.WriteLine();
        }
    }
}
