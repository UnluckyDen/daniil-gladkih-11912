﻿using System;
/*
*@author Gladkih Daniil
*11912
*task wtraf 3
*/

namespace wtraf3
{
    class Program
    {
        static void Main(string[] args)
        {
            int m = Convert.ToInt32(Console.ReadLine());
            int n = Convert.ToInt32(Console.ReadLine());
            if (m == 0 || n == 0)
            {
                return;
            }
            int[,] matr = new int[m, n];
            Random rnd = new Random();
            Console.WriteLine();
            for (int x = 0; x < m; x++)
            {
                for (int y = 0; y < n; y++)
                {
                    matr[x, y] = rnd.Next(0, 10);
                    Console.Write("{0} ", matr[x, y]);
                }
                Console.WriteLine();
            }
            Pomena(matr, m, n);
            int[,] a = matr;
            Console.WriteLine();
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write("{0} ", a[i, j]);
                }
                Console.WriteLine();
            }
            static int Pomena(int[,] matr, int m, int n)
            {
                int max = 0, min = 0, sum = 0, jmax = 0, jmin = 0, tmp;
                for (int j = 0; j < n; j++)
                {
                    for (int i = 0; i < m; i++)
                    {
                        sum += matr[i, j];
                        if (sum > max)
                        {
                            max = sum;
                            sum = 0;
                            jmax = j;
                        }
                        if (sum < min)
                        {
                            min = sum;
                            sum = 0;
                            jmin = j;
                        }
                    }
                }
                for (int j = 0; j < n; j++)
                {
                    for (int i = 0; i < m; i++)
                    {
                        tmp = matr[i, jmax];
                        matr[i, jmax] = matr[i, jmin];
                        matr[i, jmin] = tmp;
                    }
                }
                return matr[m-1, n-1];
            }
        }
    }
}

