﻿using System;
/*
 *@author Gladkih Daniil
 *11912
 *Task 25
 */

namespace Task25
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine();
            int n = Convert.ToInt32(Console.ReadLine());
            int[,] a = new int[n, n];
            if (n <= 0)
            {
                Console.WriteLine();
                return;
            }
            int[] sum = new int[2 * n - 1];
            Random rnd = new Random();
            int k, imax = 0, max = 0;

            #region matrix
            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    a[i, j] = rnd.Next(1, 10);
                    Console.Write(" " + a[i, j]);
                }
                Console.WriteLine();
            }
            #endregion
            for (k = 0; k < n; k++)
            {
                for (int i = 0; i < n; i++)
                {
                    for (int j = n - 1; j >= 0; j--)
                    {
                        if (i + k == j)
                            sum[k] += a[i, j];
                        else if (j + k == i)
                            sum[n - 1 + k] += a[i, j];
                    }
                }
            }
            for (int i = 0; i < 2 * n - 1; i++)
            {
                if (max < sum[i])
                {
                    imax = i;
                    max = sum[i];
                }
            }
            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (imax < n - 1)
                    {
                        if (i + imax == j)
                        {
                            Console.Write(a[i, j] + " ");
                        }
                    }
                    else
                    {
                        if (j + imax - n + 1 == i)
                        {
                            Console.Write(a[i, j] + " ");
                        }
                    }
                }
            }
            Console.WriteLine();
        }
    }
}


