﻿using System;
/*
*@author Daniil Gladkih
*11912
*task
*/

namespace _27
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("lines 1");
            int m = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("columns 1");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("lines 2");
            int k = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("columns 2");
            int t = Convert.ToInt32(Console.ReadLine());
            Random rnd = new Random();
            if (m <= 0 || n <= 0 || k <= 0 || t <= 0)
            {
                Console.WriteLine("Set positive size of matrix");
                return;
            }
            if (n != k)
            {
                Console.WriteLine("columns in 1 matrix must be = lines in 2 matrix");
                return;
            }
            int[,] a = new int[m, n];
            int[,] b = new int[k, t];
            int[,] c = new int[m, t];
            if (a == null || b == null || c == null)
            {
                Console.WriteLine("Too much memory. Try to use smaller size");
                return;
            }
            Console.WriteLine("Matrix 1");
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    a[i, j] = rnd.Next(0, 10);
                    Console.Write("{0} ", a[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("Matrix 2");
            for (int i = 0; i < k; i++)
            {
                for (int j = 0; j < t; j++)
                {
                    b[i, j] = rnd.Next(0, 10);
                    Console.Write("{0} ", b[i, j]);
                }
                Console.WriteLine();
            }
            for (int i = 0; i < m; i++)
                for (int j = 0; j < t; j++)
                {
                    int s = 0;
                    for (int p = 0; p < n; p++)
                        s = s + a[i, p] * b[p, j];
                    c[i, j] = s;
                }
            Console.WriteLine("New matrix");
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < t; j++)
                    Console.Write("{0} ", c[i, j]);
                Console.WriteLine();
            }
        }
    }
}

