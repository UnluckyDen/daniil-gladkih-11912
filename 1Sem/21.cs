﻿using System;
/*
*@author Daniil Gladkih
*11912
*task 21
*/

namespace _21
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int n = 100,middle,left,right;
            int k = Convert.ToInt32(Console.ReadLine());
            int[] a = new int [n] ;
            right = n;
            left = 0;
            middle = left + (right - left) / 2;
            for (int i = 0; i < n; i++)
            {
                a[i] = i;
            }
            int j = 0;
            while (left<=right)
            {
                if (a[middle] < k)
                {
                    left = middle + 1;
                    middle = left + (right - left) / 2;
                }
                else
                {
                    if (a[middle] > k)
                    {
                        right = middle - 1;
                        middle = left + (right - left) / 2;
                    }
                    else
                    {
                        Console.WriteLine(a[middle]);
                        break;
                    }
                    j++;
                }
            }
        }
    }
}
