﻿using System;
/*
*@author Daniil Gladkih
*11912
*task 22
*/

namespace _22
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int[] a = new int[n];
            int q, min = 0, max = 0;
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }
            if (a.Length == 1)
                Console.WriteLine(a);
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] < a[min])
                    min = i;
                else if (a[i] > a[max])
                    max = i;
            }
            if (max < min)
            {
                q = max;
                max = min;
                min = q;
            }
            int[] b = new int[a.Length - (max - min - 1)];
            int j = 0;
            for (int i = 0; i <= min; ++i, ++j)
                b[j] = a[i];
            for (int i = max; i < a.Length; ++i, ++j)
                b[j] = a[i];
            for (int i=0;i<b.Length;i++)
            {
                Console.Write(b[i]+" ");
            }
            Console.WriteLine();
        }
    }
}
