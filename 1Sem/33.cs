﻿using System;
/*
* @author Gladkih Daniil
* 11912
* Task 33
*/

namespace Task33
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            n = Factorial(n);
            Console.WriteLine(n);
        }
        static int Factorial(int n)
        {
            if (n == 0)
            {
                return 1;
            }
            else
            {
                return n * Factorial(n - 1);
            }
        }

    }

}
