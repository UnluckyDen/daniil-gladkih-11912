﻿using System;
/*
*@author Daniil Gladkih
*11912
*task C5
*/

namespace C5
{
    class C5
    {
        static bool ProverkaMatrici(int[,] arr, int m, int n)
        {
            if (m != n)
                return false;

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (j == i)
                    {
                        int b = 0;
                        for (int l = 0; l < m; l++)
                            b += Math.Abs(arr[l, j]);
                        if (Math.Abs(arr[i, j]) < b)
                            return false;
                    }
                }
            }
            return true;
        }
        static void Main(string[] args)
        {
            int m = int.Parse(Console.ReadLine());
            int n = int.Parse(Console.ReadLine());
            int[,] arr = new int[m, n];
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    arr[i, j] = int.Parse(Console.ReadLine());
                }
            }
            bool answer = ProverkaMatrici(arr, m, n);
            if (answer == true)
                Console.WriteLine("+");
            else
                Console.WriteLine("-");
            Console.ReadKey();
        }
    }
}
