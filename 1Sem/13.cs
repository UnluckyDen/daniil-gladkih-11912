﻿/*
*@author Daniil Gladkih
*11912
*task 13
*/
using System;

namespace _13
{
    class Program
    {
        static void Main()
        {
            int chislo = Convert.ToInt32(Console.ReadLine()), summa1 = 0, summa2 = 0, q1, w1, i = 0;
            q1 = chislo / 1000;
            w1 = chislo % 1000;
            while (i != 3)
            {
                summa1 = summa1 + (q1 % 10);
                q1 = q1 / 10;
                summa2 = summa2 + (w1 % 10);
                w1 = w1 / 10;
                i++;
            }
            if (summa1 == summa2) Console.WriteLine("Amounts are equal " + summa1 + "=" + summa2);
            else Console.WriteLine("Amounts are not equal "+summa1+"!="+summa2);
        }
    }
}