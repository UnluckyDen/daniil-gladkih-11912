﻿using System;
/*
*@author Daniil Gladkih
*11912
*task 29
*/

namespace _29
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int[,] m = new int[2 * n + 1, 2 * n + 1];
            Random random = new Random();
            for (int i=0;i<(2*n+1);i++)
            {
                for (int j = 0; j < (2*n+1); j++)
                {
                    m[i, j] = random.Next(1,9);
                }
            }
            MatrixPrint(ref m,ref n);
            Console.WriteLine();
            MatrixTreugolnik(ref m , ref n);
            Console.WriteLine();
        }
        static void MatrixPrint(ref int[,] m ,ref int n)
        {
            for (int i = 0; i<(2*n+1);i++)
            {
                Console.WriteLine();
                for (int j = 0; j<(2*n+1); j++)
                {
                    Console.Write( m[i,j] + " ");
                }
            }
        }

        static void MatrixTreugolnik(ref int[,] m, ref int n)
        {
            for (int i = 0; i < (2 * n + 1); i++)
            {
                Console.WriteLine();
                for (int j = 0; j < (2 * n + 1); j++)
                {
                    if ((i>j && (i+j)>(2*n)) || (i<j && (i+j)<(2*n)))
                        m[i, j] = 0;
                    Console.Write(m[i, j] + " ");
                }
            }
        }
    }
}
