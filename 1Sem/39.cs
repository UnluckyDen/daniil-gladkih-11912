﻿using System;
/*
*@author Daniil Gladkih
*11912
*task 39
*/

namespace _38
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            string[] a = new string[n];   
            zapoln(a);
            sort(a);
            vivod(a);

        }
        static void sort(string[] a)
        {

            Array.Sort(a);                                 
        }
        static void zapoln(string[] a)
        {
            for (int i = 0; i < a.Length; i++)
            a[i] = Console.ReadLine();
        }
        static void vivod(string[] a)
        {
            for (int i =0; i<a.Length;i++)
                Console.Write(a[i] + " ");
        }
    }
}