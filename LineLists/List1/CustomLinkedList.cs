﻿using System;
using System.Collections.Generic;
using System.Text;

namespace List
{
    /// <summary>
    /// Двунаправленный связный список
    /// </summary>
    public class CustomLinkedList<T> : ICustomCollection<T>
        where T : IComparable<T>
    {
        private LinkedNode<T> head;

        ///<inheritdoc/>
        public void Add(T item)
        {
            if (IsEmpty())
            {
                head = new LinkedNode<T>() { Data = item };
            }
            else
            {
                var node = head;
                while(node.NextNode!=null)
                {
                    node = node.NextNode;
                }
                var newNode = new LinkedNode<T>() { Data = item, PrevNode = node };
                node.NextNode = newNode;
            }
        }
        ///<inheritdoc/>
        public void AddRange(T[] items)
        {
            if (items.Length != 0)
            {
                for (int i = 0; i < items.Length; i++)
                {
                    Add(items[i]);
                }
            }
            else
                //todo homework
                throw new NotImplementedException("Массив подаваемый на вход пуст");
        }
        ///<inheritdoc/>
        public void Clear()
        {
            //todo homework
            head = null;
        }
        ///<inheritdoc/>
        public bool Contains(T data)
        {
            LinkedNode<T> curNode = head;

            while (curNode != null)
                if (curNode.Data.CompareTo(data) == 0)
                    return true;
                else
                    curNode = curNode.NextNode;

            return false;
        }
        ///<inheritdoc/>
        public int IndexOf(T item)
        {
            if (head != null && item != null)
            {
                LinkedNode<T> node = head;
                int count = 0;
                while (node.Data.CompareTo(item) != 0)
                {
                    node = node.NextNode;
                    count++;
                }
                return count;
            }
            else
                //todo homework
                throw new NotImplementedException("Какая-то ошибка(элемент=null либо список пуст)");
        }
        ///<inheritdoc/>
        public void Insert(int index, T item)
        {
            var node = head;
            int i = 1;
            if (index == 0)
            {
                head.Data = item;
                head.NextNode = node;
            }
            else
            {
                LinkedNode<T> prev = head;
                node = node.NextNode;
                while (node.NextNode != null && i < index)
                {
                    prev = node;
                    node = node.NextNode;
                    i++;
                }
                if (i == index)
                {
                    LinkedNode<T> vstavka = new LinkedNode<T> { Data = item, NextNode = node, PrevNode = node.PrevNode };
                    prev.NextNode = vstavka;
                }
                else if (i + 1 == index) { node.NextNode.Data = item; }
                else throw new IndexOutOfRangeException();
            }
        }

        ///<inheritdoc/>
        public bool IsEmpty()
        {
            return head == null;
        }
        ///<inheritdoc/> 
        public void Remove(T item)
        {
            //todo homework
            //проверка списка на пустоту
            if (IsEmpty())
            {
                return;
            }

            //Значение - головной элемент
            if (head.Data.CompareTo(item) == 0)
            {
                head = head.NextNode;
                head.PrevNode = null;
                return;
            }

            //идем до конца списка, рассматриваем 2 случая:

            //значение отсутствует, возвращаем сообщение об отсутствии эл-та
            //удаляем соответствующий элемент
            LinkedNode<T> prevNode = null;
            LinkedNode<T> node = head;
            bool found = false;

            while (node != null && !found)
            {
                if (node.Data.CompareTo(item) == 0)
                    found = true;
                else
                {
                    prevNode = node;
                    node = node.NextNode;
                }
            }
            if (found)
            {
                prevNode.NextNode = node.NextNode;
                node.PrevNode = node.PrevNode.PrevNode;
            }

            return;
        }

        ///<inheritdoc/>
        public void RemoveAll(T item)
        {
            //todo homework
            LinkedNode<T> node = head;
            while (node != null)
            {
                Remove(item);
                node = node.NextNode;
            }
        }
        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            //проверка на выход за границы коллекции
            if (index < 0 || index > (Size() - 1))
                throw new IndexOutOfRangeException();


            //Удаление первого элемента
            if (index == 0)
            {
                head = head.NextNode;
                head.PrevNode = null;
            }
            else
            {
                var node = head;
                for (var i = 0; i < index; i++) {
                    node = node.NextNode;
                }
                node.NextNode = node.NextNode.NextNode;
                node.NextNode.PrevNode = node;
            }
        }
        ///<inheritdoc/>
        public void Reverse()
        {
            //по желанию
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public int Size()
        {
            if (head != null)
            {
                var  node = head;
                int count = 0;
                while (node != null)
                {
                    count++;
                    node = node.NextNode;
                }
                return count;
            }
            else
            {
                throw new Exception("Лист пуст");
            }
            //todo homework
        }
    }
}
