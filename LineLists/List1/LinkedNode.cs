﻿using System;
using System.Collections.Generic;
using System.Text;

namespace List
{
    /// <summary>
    /// Элемент двунаправленного линейного списка
    /// </summary>
    public class LinkedNode<T>
    {
        /// <summary>
        /// Ссылка на предыдущий элемент
        /// </summary>
        public LinkedNode<T> PrevNode { get; set; }
        public LinkedNode<T> NextNode { get; set; }
        public T Data { get; set; }
    }
}
