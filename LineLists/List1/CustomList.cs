﻿using System;
using System.Collections.Generic;
using System.Text;

namespace List
{
    /// <summary>
    /// Линейный односвязный список
    /// </summary>
    public class CustomList<T> : ICustomCollection<T> where T: IComparable<T>
    {
        private Node<T> head;
        ///<inheritdoc/>
        public void Add(T item)
        {
            if (IsEmpty()) head = new Node<T>() {Data = item};
            else
            {
                Node<T> node = head;
                while (node.NextNode != null) node = node.NextNode;
                node.NextNode = new Node<T>() { Data = item };
            }
        }
        ///<inheritdoc/>
        public void AddRange(T[] items)
        {
            if (items.Length != 0)
            {
                for (int i = 0; i < items.Length; i++)
                {
                    Add(items[i]);
                }
            }
            else
            //todo homework
            throw new NotImplementedException("Массив подаваемый на вход пуст");
        }
        ///<inheritdoc/>
        public void Clear()
        {
            head = null;
            //todo homework
        }
        ///<inheritdoc/>
        public bool Contains(T data)
        {
            Node<T> curNode = head;

            while (curNode != null)
                if (curNode.Data.CompareTo(data) == 0)
                    return true;
                else
                    curNode = curNode.NextNode;

            return false;
        }
        ///<inheritdoc/>
        public int IndexOf(T item)
        {
            if (head != null && item != null) 
            {
                Node<T> node = head;
                int count = 0;
                while (node.Data.CompareTo(item) != 0)
                {
                    node = node.NextNode;
                    count++;
                }
                return count;
            }
            else
            //todo homework
            throw new NotImplementedException("Какая-то ошибка(элемент=null либо список пуст)");
        }
        ///<inheritdoc/>
        public void Insert(int index, T item)
        {
            {

                Node<T> node = head;
                int i = 1;
                if (index == 0)
                {
                    head.Data = item;
                    head.NextNode = node;
                }
                else
                {
                    Node<T> prev = head;
                    node = node.NextNode;
                    while (node.NextNode != null && i < index)
                    {
                        prev = node;
                        node = node.NextNode;
                        i++;
                    }
                    if (i == index)
                    {
                        Node<T> vstavka = new Node<T> { Data = item, NextNode = node };
                        prev.NextNode = vstavka;
                    }
                    else if (i + 1 == index) { node.NextNode.Data = item; }
                    else throw new IndexOutOfRangeException();
                }
                //todo homework
            }
        }
        ///<inheritdoc/>
        public bool IsEmpty()
        {
            return head == null;
        }
        ///<inheritdoc/>
        public void Remove(T item)
        {
            //проверка списка на пустоту
            if (IsEmpty())
            {
                return;
            }

            //Значение - головной элемент
            if (head.Data.CompareTo(item) == 0)
            {
                head = head.NextNode;
                return;
            }

            //идем до конца списка, рассматриваем 2 случая:

            //значение отсутствует, возвращаем сообщение об отсутствии эл-та
            //удаляем соответствующий элемент
            Node<T> prevNode = null;
            Node<T> node = head;
            bool found = false;

            while(node != null && !found) {
                if(node.Data.CompareTo(item) == 0)
                    found = true;                                   
                else
                {
                    prevNode = node;
                    node = node.NextNode;
                }
            }
            if (found) prevNode.NextNode = node.NextNode;

            return;
        }
        ///<inheritdoc/>
        public void RemoveAll(T item)
        {
            if (IsEmpty())
            {
                return;
            }

            if (head.Data.CompareTo(item) == 0)
            {
                head = head.NextNode;
                return;
            }
            ///Node<T> prevNode = null;
            Node<T> prev = null;
            Node<T> node = head;

            while (node != null)
            {
                if (node.Data.CompareTo(item) == 0)
                {
                    prev.NextNode = node.NextNode;
                    node = node.NextNode; 
                }
                else
                {
                    prev = node;
                    node = node.NextNode;
                }
            }

            return;
            //todo homework
        }
        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            //проверка на выход за границы коллекции
            if (index < 0 || index > (Size() - 1))
                throw new IndexOutOfRangeException();

            if (index == 0)
            {
                head = head.NextNode;
            }
            Node<T> node = head.NextNode;
            Node<T> prev = null;
            int i = 0;
            while (node != null && i < (index))
            {
                prev = node;
                node = node.NextNode;
                i++;
            }
            if (i == (index))
            {
                
                prev.NextNode = node.NextNode;
            }
            else throw new IndexOutOfRangeException();
            //todo homework

        }
        ///<inheritdoc/>
        public void Reverse()
        {
                if (this.head != null)
                {
                Node<T> cur = head;
                Node<T> after = head.NextNode;
                head.NextNode = null;
                    while (after != null)
                    {
                    Node<T> p = after.NextNode;
                    after.NextNode = cur;
                    cur = after;
                    after = p;
                    }
                head = cur;               
                }
            else
            {
                throw new NotImplementedException("Список пуст");
            }            
            //todo homework
        }
        ///<inheritdoc/>
        public int Size()
        {
            //todo homework
            if (head != null) 
                {
                Node<T> node = head;
                int count = 0;
                while (node != null) 
                {
                    count++;
                    node = node.NextNode;
                }
                return count;
            }
            else
            {
                throw new Exception("Лист пуст");
            }
        }
        ///<inheritdoc/>
        public void Display()
        {
            if (!IsEmpty())
            {
                Node<T> node = head;
                while (node != null)
                {
                    Console.Write(node.Data + " ");
                    node = node.NextNode;
                }

                Console.WriteLine();
            }
            else Console.WriteLine("list empty");
        }
    }
}
