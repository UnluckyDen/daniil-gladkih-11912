﻿using System;

namespace List
{
    /// <summary>
    /// Узел списка 
    /// </summary>
    public class Node<U>
    {
        /// <summary>
        /// Информационное поле
        /// </summary>
        public U Data { get; set; }

        /// <summary>
        /// Ссылка на следующий узел
        /// </summary>
        public Node<U> NextNode { get; set; }
    }
}
