﻿/*
*@author Daniil Gladkih
*11912
*task LineLists
*/
using System;

namespace Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            var runner = new Runner();
            runner.Run();
        }
    }
}
