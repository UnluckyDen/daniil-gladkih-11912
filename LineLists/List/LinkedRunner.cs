﻿using System;
using System.Collections.Generic;
using System.Text;
using List;

namespace Runner
{
    /// <summary>
    /// Примеры класса связного двунаправленного списка
    /// </summary>
    public class LinkedRunner
    {
        public void Run() 
        {
            var list = new CustomLinkedList<string>();
            list.Add("Hello");
            list.Add(", ");
            list.Add("world");
            list.Add("!");
        }
    }
}
